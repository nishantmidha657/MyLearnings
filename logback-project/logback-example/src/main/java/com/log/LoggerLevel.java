package com.log;
import ch.qos.logback.classic.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerLevel {


	
	public static void main(String[] args) {
		
		//trace<debug<info<warn<error
		
		
		// get a logger instance named "com.log". Let us further assume that the
		// logger is of type  ch.qos.logback.classic.Logger so that we can
		// set its level
		ch.qos.logback.classic.Logger logger = 
		        (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.log");
	//set its Level to INFO. The setLevel() method requires a logback logger
	logger.setLevel(Level.DEBUG);
	

	Logger logger1 = LoggerFactory.getLogger("com.log.LoggerLevel");

	// This request is enabled, because WARN >= INFO
	logger.warn("Low fuel level.");

	// This request is disabled, because DEBUG < INFO. 
	logger.debug("Starting search for nearest gas station.");

	// The logger instance barlogger, named "com.foo.Bar", 
	// will inherit its level from the logger named 
	// "com.foo" Thus, the following request is enabled 
	// because INFO >= INFO. 
	logger1.info("Located nearest gas station.");

	// This request is disabled, because DEBUG < INFO. 
	logger1.debug("Exiting gas station search");
	
	}
}
