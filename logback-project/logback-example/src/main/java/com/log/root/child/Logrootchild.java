package com.log.root.child;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.log.TestJaronConfig;

public class Logrootchild {
	final static Logger logger = LoggerFactory.getLogger(TestJaronConfig.class);
	public static void main(String[] args) {
		logger.info("Info logger");
		logger.debug("debug logger");
		logger.error("error logger");
		logger.trace("Trace logger");
		
	}
}
