package com.log.root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.log.TestJaronConfig;

public class TestRootLevel {
	final static Logger logger = LoggerFactory.getLogger(TestRootLevel.class);
	public static void main(String[] args) {
		logger.info("Info logger");
		logger.debug("debug logger");
		logger.error("error logger");
		logger.trace("Trace logger");
		
	}
}
