package com.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

public class PrintStatus {
	 public static void main(String[] args) {
	Logger logger = LoggerFactory.getLogger(PrintStatus.class);
    logger.debug("Hello world.");


    // print internal state
    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    StatusPrinter.print(lc);
}
}