package com.Appenders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;

public class InterceptLogging extends RollingFileAppender<ILoggingEvent> {
	List<String> l = new ArrayList<String>();
	static HashMap<String, AtomicLong> map = new HashMap<String, AtomicLong>();

	void addKeyOrIncrement(String key) {
		if (map.get(key) == null) {
			map.put(key, new AtomicLong(1));
		} else {
			System.out.println("no");
			map.get(key).incrementAndGet();
		}

	}

	@Override
	public void writeOut(ILoggingEvent event) throws IOException {

		String Key = event.getMessage().substring(0,
				event.getMessage().indexOf(";"));
		addKeyOrIncrement(Key);
		super.writeOut(event);
	}

	public static HashMap<String, AtomicLong> getMap1() {
		return map;
	}

}
