package com.alarm.test;

import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class StatefulBasedAlarm implements AlarmCondition {
	Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	AlarmConfigHelper alarmConfigHelper;

	@Autowired
	LogMessage logMessage;
	AtomicBoolean isAlarmRaised = new AtomicBoolean();
	long currentValue;
	String alarmName;

	public StatefulBasedAlarm() {

	}

	public StatefulBasedAlarm(String alarmName) {
		// TODO Auto-generated constructor stub
		this.alarmName = alarmName;
	}

	abstract int getCurrentValue();

	abstract void resetValue();

	public String evaluateAlarmCondition() {
		// TODO Auto-generated method stub
		try {
			currentValue = getCurrentValue();
			System.out.println("*****" + currentValue);
			int threshhold = Integer.parseInt(alarmConfigHelper
					.getProperty(alarmName + ".threshold"));

			if (currentValue > threshhold) {

				isAlarmRaised.set(true);
				System.out.println("breachedddddddddddddddddds");

				logger.info(
						"1000;Raising alarm current value {} breached the configured threshold {} ",
						currentValue, threshhold);
				return String
						.format("1000;Raising alarm current value %s breached the configured threshold %s ",
								currentValue, threshhold);
			} else {
				if (isAlarmRaised.compareAndSet(true, false)) {
					resetValue();
					logger.info(
							"1001;Clearing alarm current  value {} with in the configured threshold {} now ",
							currentValue, threshhold);

				}
			}

		} catch (Exception exception) {
			logger.debug("Exception in alarm config helper: {}", exception);
		}
		return alarmName;
	}

}
