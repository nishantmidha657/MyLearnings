package com.alarm.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class ApplicationProperties extends FileWatcher {

	Logger logger = LoggerFactory.getLogger(getClass());
	static String propertyFileName = "alarm-properties.properties";
File file=new File(propertyFileName);
Properties properties;
	InputStream inputStream;
public ApplicationProperties() {
	// TODO Auto-generated constructor stub
	super(new File(propertyFileName));
}
//@Value("${alarm.alphabet.a}")
String data;
@Bean
public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer()
{
	return new PropertySourcesPlaceholderConfigurer();
}

@Bean(name="applicationProperties")
	Properties getPropertyInstance() throws IOException {
		try {
			 properties=properties = new Properties();			
			inputStream = getClass().getClassLoader().getResourceAsStream(
					propertyFileName);

			if (inputStream != null) {
				properties.load(inputStream);
	
				
			} else {
				throw new FileNotFoundException("property file '"
						+ propertyFileName + "' not found in the classpath");
			}

		} catch (Exception exception) {
			logger.info("Exception in loading Properties file: {} ", exception);
		} finally {
			inputStream.close();
		}
		return properties;
	}
/*@Value("${alarm.alphabet.a}")
void getValue(String value1)
{
System.out.println(value1);
}*/
	@Override
	protected void onChange(File newFile) {
		// TODO Auto-generated method stub
		try {
			System.out.println("yey");
			inputStream = getClass().getClassLoader().getResourceAsStream(file.getName());
			properties.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
