package com.alarm.test;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AlphabetCounterBasedAlarm extends StatefulBasedAlarm {
	static Logger logger = LoggerFactory.getLogger("AlphabetCounterBasedAlarm");
	@Autowired
	LogMessage logEvent;
	
	String alarmName;
	Pattern alarmRegex;
	AtomicInteger alarmCount = new AtomicInteger();

	public AlphabetCounterBasedAlarm(String alarmName, Pattern regEx)
			throws IOException {
		// TODO Auto-generated constructor stub
		
		super(alarmName);
		this.alarmName = alarmName;
		this.alarmRegex = regEx;
		logger.info("inside constructor");
		// counterBasedAlarm(alarmName);
	}

	@Override
	int getCurrentValue() {
		// TODO Auto-generated method stub
		return alarmCount.get();
	}

	@Override
	void resetValue() {
		// TODO Auto-generated method stub
		alarmCount.set(0);
	}

	void processAlarm(String logMessage) {
		logger.info("Inside process Alarm method()");
		logEvent.logMessage("processsing input");
		if (alarmRegex.matcher(logMessage).find()) {

			alarmCount.getAndIncrement();
			//evaluateAlarmCondition();
			logger.info("hellloooooooooooooooooooooooooooooooooooooooooooooo");
		}
	}
}
