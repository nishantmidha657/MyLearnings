package com.alarm.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Caller {
	static Logger logger = LoggerFactory.getLogger(Caller.class);


	
	public static void main(String[] args) {
		try {
			ApplicationContext context = new AnnotationConfigApplicationContext(
					AlarmConfiguration.class);
	ProcessInputData processInputData=context.getBean(ProcessInputData.class)	;	
			processInputData.getInput();
		
		} catch (Exception exception) {
			logger.debug("Exception in main class {}", exception);
		}
		finally{
		
		}
	}
}
