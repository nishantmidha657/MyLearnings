package com.alarm.test;

import java.io.IOException;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

public class AlphabetAlarmConfigurationBase {
	
	@Autowired
	AlarmConfigHelper alarmConfigHelper;
	AlphabetCounterBasedAlarm counterBasedAlarm(String alarmName) throws
	IOException {

Pattern regex = Pattern.compile(alarmConfigHelper.getProperty(alarmName
		+ ".regex"));

return new AlphabetCounterBasedAlarm(alarmName,regex);
}

}
