package com.alarm.test;

import java.io.File;
import java.util.TimerTask;

public abstract class FileWatcher extends TimerTask{
private long timeStamp;
private File file;

public FileWatcher(File file)
{
	this.file=file;
	this.timeStamp=file.lastModified();
}


@Override
public final void run()
{
	long timeStampNew=this.file.lastModified();
	if(this.timeStamp != timeStampNew)
	{
		this.timeStamp=timeStampNew;
		System.out.println("hellloo");
		onChange(this.file);
	}
}
protected abstract void onChange(File newFile);
}
