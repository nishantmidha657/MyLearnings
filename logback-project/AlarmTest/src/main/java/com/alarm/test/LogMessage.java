package com.alarm.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogMessage {

	Logger logger = LoggerFactory.getLogger("CounterLogger");

	public void logMessage(String logMessage) {
		logger.debug(logMessage);
	}
}
