package com.alarm.test;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AlarmConfigHelper {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	ApplicationProperties properties;

	void display() throws IOException {
		logger.debug("inside display method {} ", properties);
		System.out.println(properties.getPropertyInstance().getProperty(
				"alarm.alphabet.a"));
	}

	String getProperty(String alarmName) throws IOException {
		logger.debug("inside getProperty method ", properties);
		return properties.getPropertyInstance().getProperty(
				"alarm." + alarmName);
	}

}
