package com.alarm.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import javax.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

public class ProcessLoggerListnerNotifier implements LoggerListener {
	List<AlphabetCounterBasedAlarm> alarmsList = new ArrayList<AlphabetCounterBasedAlarm>();
	Pattern regex;
	String alarmName;
@PostConstruct
	void config()
{
	System.out.println("Registring listener");
	LoggingFileAppender.registerListner(this);

}
	
	@Autowired
	void getAlarms(Set<AlarmCondition> alarm) {
	
		for (AlarmCondition alarmCondition : alarm) {
	System.out.println(alarm);
			if (alarmCondition instanceof AlphabetCounterBasedAlarm) {
				AlphabetCounterBasedAlarm alarmLocal = (AlphabetCounterBasedAlarm) alarmCondition;
				alarmsList.add(alarmLocal);
			}
		}

	}



	public void notifyLogger(String logMessage) {
System.out.println("Inside notifyLogger method()");
		for (AlphabetCounterBasedAlarm alarm : alarmsList) {	
			alarm.processAlarm(logMessage);
			}

		}
	}


