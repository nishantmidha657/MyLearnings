package com.alarm.test;

import java.io.IOException;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

public class LoggingFileAppender extends FileAppender<ILoggingEvent> {

	static LoggerListener loggerListener;
	
    static	void registerListner(LoggerListener listener)
	{
		loggerListener=listener;
	}
	
	@Override
	public void writeOut(ILoggingEvent  event) throws IOException
	{System.out.println("inside() writeout method");
		String logMessage=event.getFormattedMessage();
		System.out.println("**************"+event.getFormattedMessage());
	if(logMessage.contains(";"))	
	{
		loggerListener.notifyLogger(logMessage);
		
	}
	super.writeOut(event);
	}
	
	
}
