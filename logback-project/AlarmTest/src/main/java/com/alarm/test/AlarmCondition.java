package com.alarm.test;

import java.io.IOException;

public interface AlarmCondition {
String evaluateAlarmCondition() throws IOException;
}
