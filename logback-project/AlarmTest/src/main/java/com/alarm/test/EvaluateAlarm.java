package com.alarm.test;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@Configuration
public class EvaluateAlarm extends StatefulBasedAlarm{

	Logger logger=LoggerFactory.getLogger("Alarm");
	@Autowired
	LogMessage logMessage;
	
	@Autowired
	Set <AlarmCondition> alram;
	
    @Scheduled(cron="0 * * * * *")
    void evaluateAlarmConditions()
	{
    	//logger.info(evaluateAlarmCondition());
if(alram instanceof AlphabetCounterBasedAlarm)
{
	System.out.println(((AlphabetCounterBasedAlarm) alram).evaluateAlarmCondition());
}
	}

	@Override
	int getCurrentValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	void resetValue() {
		// TODO Auto-generated method stub
		
	}
	
}
