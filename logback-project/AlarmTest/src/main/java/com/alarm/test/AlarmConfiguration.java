package com.alarm.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({AlphabetAlarmConfiguration.class,EvaluateAlarm.class,ApplicationProperties.class})
public class AlarmConfiguration {

	/*@Bean(name = "applicationProperties")
	ApplicationProperties applicationProperties() {
		System.out.println("properties loading");
		return new ApplicationProperties();
	}
*/
	@Bean(name = "alarmConfigHelper")
	AlarmConfigHelper alarmConfigHelper() {
		return new AlarmConfigHelper();
	}

	@Bean(name = "processInputData")
	ProcessInputData processInputData() {
		return new ProcessInputData();
	}
	@Bean
	LogMessage getLogMessage()
	{
		return new LogMessage();
	}
@Bean
EvaluateAlarm evaluateAlarm()
{
	return new EvaluateAlarm();
}
}
