package com.alarm.test;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlphabetAlarmConfiguration extends AlphabetAlarmConfigurationBase {
public static final String ALPHABET_A="alphabet.a";
	Logger logger=LoggerFactory.getLogger(getClass());

@Bean
ProcessLoggerListnerNotifier getProcessLoggerListnerNotifier()
{
	return new ProcessLoggerListnerNotifier();
}
    @Bean
	AlarmCondition alphabetcounterAlarmForA() throws IOException 
	{
		return counterBasedAlarm(ALPHABET_A);
	}
	
}
