package test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ConfigurationClass {
	
@Bean
DemoSingleton getDemoSingleton()
{
	return new DemoSingleton();
}

@Bean
@Scope("prototype")
DemoPrototype getDemoPrototype()
{
	return new DemoPrototype();
}

@Bean
DemoPrototypeManager getDemoPrototypeManager()
{
	return new DemoPrototypeManager() {
		
		@Override
		public DemoPrototype getProtoType() {
			// TODO Auto-generated method stub
			return getDemoPrototype();
		}
	};
	
}

}
