package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class DemoSingleton  implements ApplicationContextAware{
Logger logger=LoggerFactory.getLogger(this.getClass());
	//@Autowired
	DemoPrototype demoPrototype;
	public DemoSingleton() {
		// TODO Auto-generated constructor stub
		System.out.println("singleton instance created ");
	}
	ApplicationContext applicationContext;
	
	@Autowired
	DemoPrototypeManager demoPrototypeManager;
	

	AbstractPrototype abstractPrototype;
	

	public AbstractPrototype getAbstractPrototype() {
		return abstractPrototype;
	}

	public void setAbstractPrototype(AbstractPrototype abstractPrototype) {
		this.abstractPrototype = abstractPrototype;
	}

	public void setDemoPrototype(DemoPrototype demoPrototype)
	{
		this.demoPrototype=demoPrototype;
	}

	public 	DemoPrototype getDemoPrototype()
	{
		return demoPrototype;
	}

	public void callPrototypeMethod() {
		System.out.println("Inside sigleton class");
		////demoPrototype.handlePrototypeInstance(demoPrototype);
		//demoPrototype=applicationContext.getBean(DemoPrototype.class);
		//demoPrototype.handlePrototypeInstance(demoPrototype);
//demoPrototype=demoPrototypeManager.getProtoType();
//System.out.println("using manager class");

//demoPrototype.handlePrototypeInstance(demoPrototype);
		logger.info("********{}",demoPrototype);
		demoPrototype=abstractPrototype.getPrototype();
		demoPrototype.handlePrototypeInstance(demoPrototype);
	}

	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext=arg0;
		System.out.println(applicationContext);
	}
}
