package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Annotation;

import lOOKUPANNOTAION.ConfigurationClass;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
public static void main(String[] args) throws IOException {
//	ApplicationContext context=new AnnotationConfigApplicationContext(ConfigurationClass.class);
ApplicationContext context=new ClassPathXmlApplicationContext("Spring-conf.xml");
	DemoSingleton demoSingleton=context.getBean(DemoSingleton.class);
	BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(System.in));
String s;
	s=	bufferedReader.readLine();
	while(!s.equals("n")){
	demoSingleton.callPrototypeMethod();
	s=bufferedReader.readLine();
	}
	System.out.println("Annotationconfig");
	AnnotationConfigApplicationContext annotationConfigApplicationContext=new AnnotationConfigApplicationContext();
	annotationConfigApplicationContext.register(ConfigurationClass.class);
	annotationConfigApplicationContext.scan("test");
	annotationConfigApplicationContext.refresh();
	System.out.println("$$$$$"+ annotationConfigApplicationContext.getBean("getDemoSingleton"));
	
}
}
