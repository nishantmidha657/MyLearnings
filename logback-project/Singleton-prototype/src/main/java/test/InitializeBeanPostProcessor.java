package test;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;

public class InitializeBeanPostProcessor implements BeanPostProcessor,BeanFactoryPostProcessor,Ordered,ApplicationContextAware{

	ApplicationContext applicationContext;
	public Object postProcessAfterInitialization(Object arg0, String arg1)
			throws BeansException {
		// TODO Auto-generated method stub
		return arg0;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("Bean ''" + beanName + "'' created : " + bean.toString());
		return bean;
	}

	public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		//AutowiredAnnotationBeanPostProcessor  annotationBeanPostProcessor=new AutowiredAnnotationBeanPostProcessor();
		//annotationBeanPostProcessor.setRequiredParameterValue(false);
		//arg0.addBeanPostProcessor(annotationBeanPostProcessor);
		
		System.out.println("********"+arg0);
	
	}
	
	
	public int getOrder() {
		// TODO Auto-generated method stub
	
		return 1;
	}

	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		
	
		this.applicationContext=arg0;
		
	}

}
