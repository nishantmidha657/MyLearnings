package lOOKUPANNOTAION;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Scope("session")
public @interface sessionscope {
	ScopedProxyMode proxyMode() default ScopedProxyMode.DEFAULT;
}
