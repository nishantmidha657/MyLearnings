package lOOKUPANNOTAION;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ConfigurationClass {
	
@Bean
DemoSingleton getDemoSingleton()
{
	return new DemoSingleton();
}

@Bean
@Scope("prototype")
DemoPrototype getDemoPrototype()
{
	return new DemoPrototype();
}



}
