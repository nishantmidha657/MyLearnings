package AbstractFactoryPattern;

public class ColorFactory implements AbstractFactory {

	public Color getColor(String color){
		if(color.equals("red")){
			return new RedColor();
		}else{
			System.out.println("Color not found");
		return null;
		}
	}

	public Shape getShape(String shape) {
		// TODO Auto-generated method stub
		return null;
	}
}
