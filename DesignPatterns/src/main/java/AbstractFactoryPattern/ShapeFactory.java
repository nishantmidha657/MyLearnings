package AbstractFactoryPattern;

public class ShapeFactory implements AbstractFactory {

	public Shape getShape(String shape){
		if(shape.equals("rectangle")){
			return new Rectangle();
		}else{
			return null;
		}
	}

	public Color getColor(String color) {
		// TODO Auto-generated method stub
		return null;
	}
}
