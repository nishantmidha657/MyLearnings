package AbstractFactoryPattern;

public class Demo {
public static void main(String[] args) {
	AbstractFactory shape=FactoryProducer.getFactory("shape");
	
	shape.getShape("rectangle").draw();
	
	FactoryProducer.getFactory("color").getColor("red").fill();
}
}
