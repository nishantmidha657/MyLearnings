package MediatorPattern;

public class Demo {

	public static void main(String[] args) {
		Mediator mediator=new ChatMediator();
		User user=new UserImpl(mediator, "Nishant");
		User user2=new UserImpl(mediator, "Harpreet");
		User user3=new UserImpl(mediator, "Ashish");
		User user4=new UserImpl(mediator, "Praveen");
		User user5=new UserImpl(mediator, "Pragya");
		User user6=new UserImpl(mediator, "Girish");
		User user7=new UserImpl(mediator, "Richa");
		User user8=new UserImpl(mediator, "Arunava");
		
		mediator.addUser(user);
		mediator.addUser(user2);
		mediator.addUser(user3);
		mediator.addUser(user4);
		
		mediator.addUser(user5);
		mediator.addUser(user6);
		mediator.addUser(user7);
		mediator.addUser(user8);
		user.sendMesage("Hi All !");
		
	}
}
