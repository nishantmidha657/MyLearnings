package MediatorPattern;

public abstract class User {

	String name;
	User(String name){
		this.name=name;
	}
	abstract void sendMesage(String msg);
	abstract void receiveMessage(String msg,String name);
}
