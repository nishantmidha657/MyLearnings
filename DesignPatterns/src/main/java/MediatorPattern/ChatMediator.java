package MediatorPattern;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator implements Mediator {

	List<User> users=new ArrayList<User>();
	
	public void sendMessage(String msg,User user) {
		
		for(User u:users){
			if(u != user){
				u.receiveMessage(msg,u.name);
			}
		}
	}

	public void addUser(User user) {
		// TODO Auto-generated method stub
		users.add(user);
	}

}
