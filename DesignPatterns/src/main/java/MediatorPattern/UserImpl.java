package MediatorPattern;

public class UserImpl extends User{

	Mediator mediator;
	public UserImpl(Mediator mediator,String user) {
		super(user);
		this.mediator = mediator;
		this.name = user;
	}
	public void sendMesage(String msg) {
		System.out.println(name+":Sending Message: "+msg);
		mediator.sendMessage(msg, this);
	}

	public void receiveMessage(String msg,String name) {
		System.out.println(name +":Received Message: "+msg);
		
	}

}
