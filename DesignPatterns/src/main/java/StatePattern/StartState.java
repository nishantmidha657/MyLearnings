package StatePattern;

public class StartState implements State{

	public void doAction() {
		System.out.println("Start state");
		
	}

}
