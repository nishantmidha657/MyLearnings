package StatePattern;

public interface State {
void doAction();
}
