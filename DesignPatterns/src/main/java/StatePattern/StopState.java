package StatePattern;

public class StopState implements State{

	public void doAction() {
		System.out.println("Stop state");
	}

}
