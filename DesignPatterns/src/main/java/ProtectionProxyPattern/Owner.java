package ProtectionProxyPattern;

public class Owner {

	private boolean isOwner = true;

	private ReportGeneratorProxy reportGenerator;


	public void setReportGenerator(ReportGeneratorProxy reportGenerator) {

		this.reportGenerator = reportGenerator;

	}
	public boolean isOwner() {

		return isOwner;

	}

	public String generateDailyReport() {

		try {

			return reportGenerator.generateDailyReport();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return "";

	}

}
