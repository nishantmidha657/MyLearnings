package ProtectionProxyPattern;

public interface ReportGeneratorProxy {

	public String generateDailyReport();
}
