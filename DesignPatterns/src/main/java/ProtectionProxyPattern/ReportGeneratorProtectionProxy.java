package ProtectionProxyPattern;

import RemoteProxyPattern.ReportGenerator;

public class ReportGeneratorProtectionProxy implements ReportGeneratorProxy {

	// ReportGenerator reportGenerator;
	ReportGeneratorProxy generatorProxy;
	Staff staff;

	public ReportGeneratorProtectionProxy(Staff staff) {

		this.staff = staff;

	}

	public String generateDailyReport() {

		if (staff.isOwner()) {

			ReportGenerator reportGenerator = null;

			try {

				/*
				 * reportGenerator = (ReportGenerator) Naming
				 * .lookup("rmi://127.0.0.1/PizzaCoRemoteGenerator");
				 */

				// return staff.generateDailyReport();
				return null;
			} catch (Exception e) {

				e.printStackTrace();

			}

			return "";

		}

		else {

			return "Not Authorized to view report.";

		}

	}

}
