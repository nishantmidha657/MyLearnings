package ProtectionProxyPattern;

public interface Staff {
	 boolean isOwner();

	 void setReportGenerator(ReportGeneratorProxy reportGenerator);

}
