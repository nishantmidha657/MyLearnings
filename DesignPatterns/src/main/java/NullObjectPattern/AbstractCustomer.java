package NullObjectPattern;

public abstract class AbstractCustomer {

	String name;
	
	abstract String getName();
}
