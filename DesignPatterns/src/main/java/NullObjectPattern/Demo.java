package NullObjectPattern;

public class Demo {
public static void main(String[] args) {
    AbstractCustomer abstractCustomer=CustomerFactory.getCustomer("Rob");
    AbstractCustomer abstractCustomer1=CustomerFactory.getCustomer("Nishant");
    AbstractCustomer abstractCustomer2=CustomerFactory.getCustomer("Kelly");
    AbstractCustomer abstractCustomer3=CustomerFactory.getCustomer("Praveen");

System.out.println(abstractCustomer.getName());
System.out.println(abstractCustomer1.getName());
System.out.println(abstractCustomer2.getName());
System.out.println(abstractCustomer3.getName());
}
}
