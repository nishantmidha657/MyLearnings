package NullObjectPattern;

public class CustomerFactory {

	static String[] names = { "Nishant", "Rob", "John" };

	public static AbstractCustomer getCustomer(String name) {
		AbstractCustomer abstractCustomer = null;
		for (String name1 : names) {
			if (name1.equalsIgnoreCase(name)) {
				abstractCustomer = new RealCustomer(name);
				return abstractCustomer;
			} 
		}
		return new NullCustomer();
	}
}
