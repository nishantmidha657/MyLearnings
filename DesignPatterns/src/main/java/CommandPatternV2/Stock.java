package CommandPatternV2;

public class Stock {

	private String name="HUDCO";

	private int i = 10;

	void buy() {
		System.out.println("Buying [" + i + "]  stock  of [" + name + "]");
	}
	
	void sell() {
		System.out.println("Selling [" + i + "]  stock  of [" + name + "]");
	}
	
	
}
