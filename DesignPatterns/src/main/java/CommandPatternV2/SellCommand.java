package CommandPatternV2;

public class SellCommand implements Command {

	Stock stock;

	public SellCommand(Stock stock) {
		this.stock = stock;
	}

	public void execute() {
		stock.sell();

	}

}
