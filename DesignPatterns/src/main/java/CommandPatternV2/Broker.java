package CommandPatternV2;

import java.util.ArrayList;
import java.util.List;

public class Broker {
	List<Command> orderList = new ArrayList<Command>();

	void placeOrder(Command order) {
		orderList.add(order);
	}

	void executeOrders() {
		for (Command command : orderList) {
			command.execute();
		}
	}
}
