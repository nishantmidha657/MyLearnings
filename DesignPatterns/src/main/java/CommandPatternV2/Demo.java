package CommandPatternV2;

public class Demo {
	public static void main(String[] args) {
		Broker broker=new Broker();
		
		broker.placeOrder(new BuyCommand(new Stock()));
		broker.placeOrder(new SellCommand(new Stock()));
		broker.executeOrders();
		
	}

}
