package CommandPatternV2;

public class BuyCommand implements Command{

	Stock stock;
	
	public BuyCommand(Stock stock) {
	this.stock = stock;
	}

	public void execute() {
		stock.buy();
		
	}
	
	
}
