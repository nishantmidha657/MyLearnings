package BridgePattern;

public abstract class Vehicle {

	VehicleAPI vehicleAPI;

	public Vehicle(VehicleAPI api) {
	
		this.vehicleAPI = api;
	}
	
	abstract void getVehicleName();
}
