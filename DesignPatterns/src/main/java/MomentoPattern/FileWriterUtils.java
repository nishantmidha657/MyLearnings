package MomentoPattern;

public class FileWriterUtils {

	private String fileName;
	private StringBuilder content;
	
	public FileWriterUtils(String fileName){
		this.fileName=fileName;
		content=new StringBuilder();
	}
	
	public String toString(){
		return content.toString();
	}
	
	public void write(String message){
		content.append(message);
	}
	
	public Momento save(){
		return new Momento(fileName, content);
	}
	
	public void undoLastState(Object obj){
		Momento momento=(Momento)obj;
		this.fileName=momento.fileName;
		this.content=momento.content;
		
	}
	private class Momento {
		StringBuilder  content;
		String fileName;
		public Momento(String fileName,StringBuilder content) {
			this.fileName=fileName;
			this.content=new StringBuilder(content);
		}
		
		
	}
	

}
