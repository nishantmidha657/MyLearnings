package MomentoPattern;

public class FileWriterCareTaker {

	Object object;
	
	void save(FileWriterUtils fileWriterUtils){
		object=fileWriterUtils.save();
	}
	
	void undoLastState(FileWriterUtils fileWriterUtils){
	fileWriterUtils.undoLastState(object);
	}
}
