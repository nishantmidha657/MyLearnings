package MomentoPattern;

public class Demo {
public static void main(String[] args) {
	FileWriterUtils fileWriterUtils=new FileWriterUtils("Demo.txt");
	
	fileWriterUtils.write("Hey this is nishant midha");
	
	System.out.println(fileWriterUtils);
	System.out.println("Saving the state");
	FileWriterCareTaker careTaker=new FileWriterCareTaker();
	careTaker.save(fileWriterUtils);
	
	System.out.println("Chaning the conetent");
	fileWriterUtils.write("Hey ! I am Nishant Midha");
	
	System.out.println(fileWriterUtils);
	
	careTaker.undoLastState(fileWriterUtils);
	System.out.println("Undo changes");
	System.out.println(fileWriterUtils);
	
	
}
}
