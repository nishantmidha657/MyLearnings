package IteratorPattern;

public class JarRepository implements Container {

	public String names[]={"Spring.jar","rt.jar","dao.jar","Nishant.jar"};

	public Iterator getIterator() {
		// TODO Auto-generated method stub
		return new JarIterator();
	}
	
	private class JarIterator implements Iterator{

		int index;
		public boolean heasNext() {
			if(index < names.length){
				return true;
			}
			return false;
		}

		public Object next() {
			if(this.heasNext()){
				return names[index++];	
			}
			return null;
		}
		
	}
}
