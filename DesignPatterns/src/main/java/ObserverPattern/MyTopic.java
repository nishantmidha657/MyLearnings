package ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class MyTopic implements Subject {

	List<Observer> observers;

	String message;

	boolean isChanged;

	public MyTopic() {
		observers = new ArrayList<Observer>();
	}

	public void register(Observer observer) {

		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	public void unRegister(Observer observer) {
		observers.remove(observer);

	}

	public void notifyObservers() {

		for (Observer observer : observers) {
			observer.update();
		}
	}

	public Object getUpdate(Observer observer) {
		// TODO Auto-generated method stub
		return this.message;
	}

	public void postMessage(String message) {
		System.out.println("Message posted to the topic :" + message);
		this.message = message;

		notifyObservers();

	}
}
