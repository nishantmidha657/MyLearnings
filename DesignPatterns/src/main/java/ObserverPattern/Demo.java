package ObserverPattern;

public class Demo {
public static void main(String[] args) {
	MyTopic myTopic=new MyTopic();
	
	Observer observer=new MyTopicSubscriber("Obj1");
	Observer observer1=new MyTopicSubscriber("Obj1");
	Observer observer2=new MyTopicSubscriber("Obj1");

	myTopic.register(observer);
	myTopic.register(observer1);
	myTopic.register(observer2);
	
	
	observer.setSubject(myTopic);
	observer1.setSubject(myTopic);
	observer2.setSubject(myTopic);
	
	observer.update();
	myTopic.postMessage("Hello");

}
}
