package ObserverPattern;

public interface Observer {

	void update();
	void setSubject(Subject subject);
}
