package FlyweightPattern;

public class Demo {
	public static void main(String[] args) {
		System.out.println("#######################################");
		SoftwareFirmType serviceFirm = SoftwareFirmFactory
				.getSoftwareFirm("service");
		System.out.println(serviceFirm.getFirmType());
		serviceFirm.addFirm(new Firm(1, "tcs"));
		System.out.println(serviceFirm.getFirmList());

		System.out.println("#######################################");
		
		SoftwareFirmType productFirm = SoftwareFirmFactory
				.getSoftwareFirm("product");
		System.out.println(productFirm.getFirmType());
		productFirm.addFirm(new Firm(2, "Adobe Systems"));
		System.out.println(productFirm.getFirmList());

		System.out.println("#######################################");
		
		SoftwareFirmType serviceFirm1 = SoftwareFirmFactory
				.getSoftwareFirm("service");
		System.out.println(serviceFirm1.getFirmType());

		System.out.println(serviceFirm1.getFirmList());

	}
}
