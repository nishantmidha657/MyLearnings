package FlyweightPattern;

import java.util.ArrayList;
import java.util.List;

public class SoftwareFirm implements SoftwareFirmType {

	String firmType;

	List<Firm> list = new ArrayList<Firm>();

	public SoftwareFirm(String firmType) {
		this.firmType = firmType;
	}

	public String getFirmType() {
		return firmType;

	}

	public List<Firm> getFirmList() {
		// TODO Auto-generated method stub
		return list;
	}

	public void addFirm(Firm firm) {
	list.add(firm);
		
	}

}
