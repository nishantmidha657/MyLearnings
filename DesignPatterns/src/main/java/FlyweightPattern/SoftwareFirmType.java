package FlyweightPattern;

import java.util.List;

public interface SoftwareFirmType {

String getFirmType();
List<Firm> getFirmList();
void addFirm(Firm firm); 
}
