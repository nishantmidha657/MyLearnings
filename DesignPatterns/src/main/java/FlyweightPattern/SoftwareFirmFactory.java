package FlyweightPattern;

import java.util.HashMap;
import java.util.Map;

public class SoftwareFirmFactory {

	final static Map<String, SoftwareFirmType> map = new HashMap<String, SoftwareFirmType>();

	static SoftwareFirmType getSoftwareFirm(String firmType) {
		SoftwareFirmType softwareFirmType = map.get(firmType);
		if (softwareFirmType == null) {
			System.out.println("Creating object for :-"+firmType);
			softwareFirmType = new SoftwareFirm("service");
			map.put(softwareFirmType.getFirmType(), softwareFirmType);

		}
		return softwareFirmType;
	}
}
