package PrototypePattern;

import java.util.Hashtable;

public class ShapeCache {

	static int i=-1;
	public static Hashtable<String , Shape> map=new Hashtable<String, Shape>();


	public static Shape getShape(String shapeId){
		if(map.containsKey(shapeId)){
			System.out.println("Already presnet ");
			Shape shape= map.get(shapeId); 
		return (Shape)shape.clone();
		}else{
			System.out.println("Creating new object");
			
			Rectangle r=new Rectangle(i++);
			map.put(""+i, r);
			return r;
		}
		
		
	}
}
