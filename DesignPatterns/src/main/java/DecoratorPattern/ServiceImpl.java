package DecoratorPattern;


public class ServiceImpl {

	
	
	
	public void getEmployeeData(){
		new EmployeeDecorator() {
			
			@Override
			void invokeMethod() {
				// TODO Auto-generated method stub
				DaoImpl daoImpl=new DaoImpl();
				System.out.println( daoImpl.getEmpData());		
			}
		}.execute();
		
	}
}
