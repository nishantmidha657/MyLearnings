package DecoratorPattern;

import java.util.List;

import javax.transaction.TransactionRolledbackException;

public abstract class EmployeeDecorator {

	abstract void invokeMethod();

	void execute() {
		try {
			System.out
					.println("Putting common exception block for each dao method to catch transaction exception to retry db operation");

			invokeMethod();
			
			System.out.println("Execution completed ");
		} catch (Exception exception) {
			if (exception instanceof TransactionRolledbackException) {
				System.out.println("Transaction rollback exception");
			}
		}

	}
}
