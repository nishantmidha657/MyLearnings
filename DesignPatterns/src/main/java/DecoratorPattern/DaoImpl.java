package DecoratorPattern;

import java.util.ArrayList;
import java.util.List;

public class DaoImpl {

	public List<Employee> getEmpData(){
		List<Employee> as=new ArrayList<Employee>();
		
		Employee employee=new Employee();
		employee.setAddress("Chandigarh");
		employee.setEmpId(1);
		employee.setName("Nishant");
		
		
		Employee employee1=new Employee();
		employee1.setAddress("Panchkula");
		employee1.setEmpId(2);
		employee1.setName("Sachin");
		
		as.add(employee1);
		as.add(employee);
		return as;
	}
}
