package BuilderPattern;

public class EmployeeBuilder {
public static void main(String[] args) {
	BuilderPattern.Employee.EmployeeBuilder emp=new Employee.EmployeeBuilder();
	emp.setAddress("chandigarh").setEmpAddress("sec4");
	emp.setEmpAddress("sec4");
	emp.setEmpId("657659");
	emp.setEmpName("Nishant");
	emp.setPhno("7087434540");
	Employee empp=emp.build();
	
	System.out.println(empp);
}
}
