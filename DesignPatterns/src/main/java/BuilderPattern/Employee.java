package BuilderPattern;

public class Employee {

	String empId;
	String empName;
	String empAddress;
	String address;
	String phno;

	private Employee(EmployeeBuilder builder) {
		// TODO Auto-generated constructor stub

		this.address=builder.address;
		this.empAddress=builder.empAddress;
		this.empId=builder.empId;
		this.empName=builder.empName;
		this.phno=builder.phno;
	}

	static class EmployeeBuilder {
		String empId;
		String empName;
		String empAddress;
		String address;
		String phno;

		public EmployeeBuilder setEmpId(String empId) {
			this.empId = empId;
			return this;
		}

		public EmployeeBuilder setEmpName(String empName) {
			this.empName = empName;
			return this;
		}

		public EmployeeBuilder setEmpAddress(String empAddress) {
			this.empAddress = empAddress;
			return this;
		}

		public EmployeeBuilder setAddress(String address) {
			this.address = address;
			return this;
		}

		public EmployeeBuilder setPhno(String phno) {
			this.phno = phno;
			return this;
		}

		Employee build() {
			return new Employee(this);
		}
	}

	public String getPhno() {
		return phno;
	}

	public String getAddress() {
		return address;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public String getEmpName() {
		return empName;
	}

	public String getEmpId() {
		return empId;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		super.toString();
		return "Employee[" + "employeeno=" + empId + ".employee address=" +address + "]";
	}
}
