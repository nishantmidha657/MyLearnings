package InterpreterPattern;

public class AndExpression implements Expression {
	Expression expression1;
	Expression expression2;

	public AndExpression(Expression expression1, Expression expression2) {
		this.expression1 = expression1;
		this.expression2 = expression2;
	}

	public boolean interpret(String context) {
		// TODO Auto-generated method stub
		return expression1.interpret(context) && expression2.interpret(context);
	}

}
