package RemoteProxyPattern;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

public class ReportGeneratorImpl extends UnicastRemoteObject implements
		ReportGenerator {

	protected ReportGeneratorImpl() throws RemoteException {

	}

	private static final long serailId = 1L;

	public String generateDailyReport() throws RemoteException {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder
				.append("**************Generating Daily Report*****************");
		stringBuilder.append("\\nLocation id :012")
				.append("\\n Today's Date " + new Date())
				.append("\\n Total pizza sell :112")
				.append("\\n Total Price : 60000");

		return stringBuilder.toString();
	}

	public static void main(String[] args) {
		try {
			ReportGenerator reportGenerator = new ReportGeneratorImpl();
			Naming.rebind("pizzaObject", reportGenerator);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}
