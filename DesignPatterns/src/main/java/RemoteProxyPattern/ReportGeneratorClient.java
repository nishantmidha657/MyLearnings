package RemoteProxyPattern;

import java.rmi.Naming;

public class ReportGeneratorClient {

	public static void main(String[] args) {
		new ReportGeneratorClient();
	}
	
	public void generateReport(){
		try {
			ReportGenerator reportGenerator=(ReportGenerator)Naming.lookup("rmi://127.0.0.1/pizzaObject");
			reportGenerator.generateDailyReport();
		} catch(Exception exception){
			exception.printStackTrace();
		}
	}
}
