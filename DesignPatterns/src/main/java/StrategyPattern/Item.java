package StrategyPattern;

public class Item {
String code;
int price;

public Item(String upc,int amount){
	this.code=upc;
	this.price = amount;
}

public String getCode(){
	return code;
}

public int getPrice(){
	return price;
}
}
