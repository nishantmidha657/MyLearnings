package StrategyPattern;

public class CardPayment extends PayStrategy {

	@Override
	void pay(int pay ) {
		System.out.println(pay +" :Paying by Card");
		
	}

}
