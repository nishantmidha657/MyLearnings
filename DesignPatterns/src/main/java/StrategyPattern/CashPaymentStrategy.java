package StrategyPattern;

public class CashPaymentStrategy extends PayStrategy{

	@Override
	void pay(int amount) {
		System.out.println(amount + ":Paying by Cash !");
	}

}
