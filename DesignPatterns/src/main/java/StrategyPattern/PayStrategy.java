package StrategyPattern;

public abstract class PayStrategy {

	abstract void pay(int amount);
}
