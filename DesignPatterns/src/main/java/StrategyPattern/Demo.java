package StrategyPattern;

public class Demo {
public static void main(String[] args) {
	ShoppingCart cart=new ShoppingCart();
	
	Item  item = new Item("1", 300);
	
	Item item2=new Item("2", 400);
	
	cart.addItem(item);
	cart.addItem(item2);
	
	
	cart.pay(new CashPaymentStrategy());
}
}
