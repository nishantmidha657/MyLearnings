package FactoryPattern;

public class ShapeFactory {

	Shape getShape(String shape)
	{
		if(shape.equals("square"))
		{
			return new Square();
		}else if(shape.equals("rectangle"))
		{
			return new Rectangle();
		}else
		{
			System.out.println("Shape is not defined");
		}
		return null;
	}
}
