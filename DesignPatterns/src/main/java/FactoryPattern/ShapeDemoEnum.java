package FactoryPattern;

import FactoryPattern.ShapeFactoryEnum.shape;

public class ShapeDemoEnum {
public static void main(String[] args) {
	ShapeFactoryEnum factory=ShapeFactoryEnum.INSTANCE;
	ShapeFactoryEnum factoryEnum=ShapeFactoryEnum.INSTANCE;
	System.out.println(factory.hashCode());
	System.out.println(factoryEnum.hashCode());
	factory.getShape(shape.Square).draw();
	
}
}
