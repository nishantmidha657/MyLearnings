package FactoryPattern;

public class ShapeDemo {
public static void main(String[] args) {
	ShapeFactory demo=new ShapeFactory();
	demo.getShape("rectangle").draw();;
	demo.getShape("square").draw();;
	demo.getShape("kuchbhi").draw();
}
}
