package FactoryPattern.vehicle;

public class TestDrive {

	public static void main(String[] args)  {
		VehicleFactory factory=VehicleFactory.INSTANCE;
		Vehicle vehicle=null;
		try {
			vehicle = factory.createVehicle(VehicleTypes.Car.name());
			vehicle=factory.createVehicle(VehicleTypes.Jeep.name());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		vehicle.drive();
	}
}
