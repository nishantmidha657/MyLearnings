package FactoryPattern;

public enum ShapeFactoryEnum {
	INSTANCE;
	enum shape{
		Square,
		Rectangle;
	};
	

	Shape getShape(shape shape)
	{
		if(shape==shape.Square)
		{
			return new Square();
		}else if(shape==shape.Rectangle)
		{
			return new Rectangle();
		}else
		{
			System.out.println("Shape is not defined");
		}
		return null;
	}
}
