package AdapterPattern;

import java.util.Random;

public class RobotEnemy {

	Random generate=new Random();
	String name;
	void move()
	{
		System.out.println("Robot enemy has taken "+generate.nextInt(10)*1);
	}
	
	void attack()
	{
		System.out.println("Robot"+ name +" has attacked "+generate.nextInt(10)*1+"enemies");
	}
	
	void setRobotName(String name)
	{
		this.name=name;
	}
}
