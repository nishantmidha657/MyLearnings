package AdapterPattern;

public interface Attack {

	void drive();
	void attack();
	void setEnemy(String name);
}
