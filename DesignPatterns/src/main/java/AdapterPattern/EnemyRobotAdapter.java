package AdapterPattern;

public class EnemyRobotAdapter implements Attack {

	RobotEnemy  robotEnemy;
	public EnemyRobotAdapter(RobotEnemy robotEnemy) {
		this.robotEnemy=robotEnemy;
		// TODO Auto-generated constructor stub
	}
	public void drive() {
		// TODO Auto-generated method stub
		robotEnemy.move();
	}

	public void attack() {
		// TODO Auto-generated method stub
		robotEnemy.attack();
	}

	public void setEnemy(String name) {
		// TODO Auto-generated method stub
		robotEnemy.setRobotName(name);
	}

}
