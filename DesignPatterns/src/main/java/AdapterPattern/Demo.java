package AdapterPattern;

public class Demo {
public static void main(String[] args) {
	EnemyAttack attack=new EnemyAttack();
	attack.setEnemy("Rahul");
	attack.drive();
	attack.attack();
	RobotEnemy robotEnemy=new RobotEnemy();
	robotEnemy.setRobotName("Sahil");
	robotEnemy.attack();
	robotEnemy.move();
	
	EnemyRobotAdapter adapter=new EnemyRobotAdapter(robotEnemy);
	adapter.setEnemy(robotEnemy.name);
	adapter.drive();
	adapter.attack();
}
}
