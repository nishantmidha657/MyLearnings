package VirtualProxyPatatern;

import java.util.List;

public class ContactListProxyImpl implements ContactList {

	ContactList contactList;

	public List<Employee> getEmployeeList() {
		if (contactList == null) {
			System.out.println("Creating object and fetching contact list ");
			contactList = new ContactListImpl();

		}
		return contactList.getEmployeeList();
	}

}
