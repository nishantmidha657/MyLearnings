package VirtualProxyPatatern;

import java.util.List;

public interface ContactList {

	List<Employee> getEmployeeList();
}
