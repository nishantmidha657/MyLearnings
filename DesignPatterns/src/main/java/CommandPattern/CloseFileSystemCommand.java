package CommandPattern;

public class CloseFileSystemCommand implements Command {

	FileSystemReceiver fileSystemReceiver;

	public CloseFileSystemCommand(FileSystemReceiver fileSystemReceiver) {
		this.fileSystemReceiver = fileSystemReceiver;
	}

	public void execute() {
		fileSystemReceiver.closeFile();

	}

}
