package CommandPattern;

public class OpenFileSystemCommand implements Command {

	FileSystemReceiver fileSystemReceiver;

	public OpenFileSystemCommand(FileSystemReceiver fileSystemReceiver) {
		this.fileSystemReceiver = fileSystemReceiver;
	}

	public void execute() {
	fileSystemReceiver.openFile();

	}
}
