package CommandPattern;

public class FileSystemClient {

	public static void main(String[] args) {
		FileSystemReceiver fileSystemReceiver=new UnixFileSystemReceiver();
		
		OpenFileSystemCommand command=new OpenFileSystemCommand(fileSystemReceiver);
		FileInvoker fileInvoker=new FileInvoker(command);
		
		fileInvoker.execute();
		
		WriteFileSystemCommand  command2=new WriteFileSystemCommand(fileSystemReceiver);
		
		fileInvoker=new FileInvoker(command2);
		
		fileInvoker.execute();
		
		CloseFileSystemCommand command3=new CloseFileSystemCommand(fileSystemReceiver);
		
		fileInvoker=new FileInvoker(command3);
		
		fileInvoker.execute();
	}
}
