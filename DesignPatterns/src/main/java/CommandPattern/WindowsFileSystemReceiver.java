package CommandPattern;

public class WindowsFileSystemReceiver implements FileSystemReceiver {

	public void openFile() {
		System.out.println("Opening file in Window file System");
		
	}

	public void writeFile() {
		System.out.println("Writing file in window file system");
		
	}

	public void closeFile() {
		System.out.println("Closing file in Windows file system");
		
	}

}
