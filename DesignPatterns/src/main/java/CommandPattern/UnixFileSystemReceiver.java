package CommandPattern;

public class UnixFileSystemReceiver implements FileSystemReceiver{

	public void openFile() {
	System.out.println("Opening file in Unix File System Receiver");
		
	}

	public void writeFile() {
		System.out.println("Writing file in Unix file system");
		
	}

	public void closeFile() {
	System.out.println("Closing file in unix file system");
		
	}

}
