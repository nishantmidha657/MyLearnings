package CommandPattern;

public class FileInvoker {

	Command command;
	
	public FileInvoker(Command command) {
	this.command=command;
	}
	
	void execute(){
		command.execute();
	}
}
