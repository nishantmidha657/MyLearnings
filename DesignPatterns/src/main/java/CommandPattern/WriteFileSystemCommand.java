package CommandPattern;

public class WriteFileSystemCommand implements Command {

	FileSystemReceiver fileSystemReceiver;
	
	public WriteFileSystemCommand(FileSystemReceiver fileSystemReceiver) {
		this.fileSystemReceiver = fileSystemReceiver;
	}
	public void execute() {
		fileSystemReceiver.writeFile();
		
	}

}
