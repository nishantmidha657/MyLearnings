package FacadePatter;

public class Square implements Shape{

	public void draw() {
		System.out.println("Square draw method called :-> drawing square");
	}

}
