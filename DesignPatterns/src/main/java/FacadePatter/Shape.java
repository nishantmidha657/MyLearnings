package FacadePatter;

public interface Shape {

	void draw();
}
