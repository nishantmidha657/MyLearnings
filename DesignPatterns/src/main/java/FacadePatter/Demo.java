package FacadePatter;

public class Demo {
public static void main(String[] args) {
	ShapeMaker smaker=new ShapeMaker();
	smaker.drawCircle();
	smaker.drawRectangle();
	smaker.drawSquare();
}
}
