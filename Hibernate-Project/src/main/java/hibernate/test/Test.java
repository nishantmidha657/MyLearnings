package hibernate.test;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;



public class Test {

	
	public static void main(String[] args) throws HibernateException{
		 SessionFactory 	sessionFactory=new Configuration().configure().buildSessionFactory();
		Course S=new Course();
		S.setCourseId(2);
		S.setCourseName("Name");
		Session s=sessionFactory.openSession();
		s.beginTransaction();
        s.save(S);
        s.getTransaction().commit();
	}

}
