package com.Appenders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;

public class InterceptLoggingStatement extends
		RollingFileAppender<ILoggingEvent> {

	List<String> l = new ArrayList<String>();
	static HashMap<String, AtomicLong> map = new HashMap<String, AtomicLong>();
	{
		l.add("10003");
		l.add("10004");
		l.add("10005");
		for (String s : l) {
			map.put(s, new AtomicLong(0));
		}
	}

	@Override
	public void writeOut(ILoggingEvent event) throws IOException {
	
		for (String s : l) {
			if (event.getMessage().startsWith(s+";")) {
				System.out.println(map.get(s));
				map.get(s).getAndIncrement();
				return;
			}
		}
		super.writeOut(event);
	}
	
	public static HashMap<String, AtomicLong> getMap1()
	{
		return map;
	}
}
