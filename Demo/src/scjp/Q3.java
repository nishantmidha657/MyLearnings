package scjp;

public class Q3 {
	void waitForSignal() throws InterruptedException {
		Object obj = new Object();
		 synchronized (Thread.currentThread()) {
		 obj.wait();
		 obj.notify();
		 }
		 }
public static void main(String[] args) throws InterruptedException {
	Q3 gh=new Q3();
	gh.waitForSignal();
}
}
//Answer java.lang.IllegalMonitorStateException