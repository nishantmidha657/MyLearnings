package scjp;

import java.io.IOException;


interface sd{
	void display();
}
class Parent {
	
	static void display()
	{
		
	}
}

class Child extends Parent {
	
	static void display()
	{
		
	}
     void say() {
    }
     public static void main(String[] args) {
		Parent p=new Child();
		p.display();
	}
}

class PovertyException extends Exception {
}

class NoFoodException extends PovertyException {
}
public class Test {
    public static void main(String[] args) throws NoFoodException, IOException {
        new Child().say();
    }
}