package scjp;

abstract class DeclareStuff {
 public static final int EASY = 3;
 abstract void doStuff(int t); }
 public class TestDeclare extends DeclareStuff {
 public static void main(String [] args) {
 int x = 5;
 new TestDeclare().doStuff(++x);
 }
void doStuff(int s) {
 s += EASY + ++s;
 System.out.println("s " + s);
 }
 } 