package scjp;

import java.io.Console;

public class Flags2 extends Thread{
private boolean isReady=false;
Console s=System.console();

public synchronized void produce() throws InterruptedException
{s.format("hello");
	isReady=true;
	System.out.println("notifying");
	wait();
	System.out.println("notification sent");
}
public synchronized void consume()
{
	while(!isReady)
	{
		try{
			System.out.println("Wait");
			notify();
			System.out.println("notifed");
		}catch(Exception e)
		{
			
		}
		isReady=false;
	}
}
public void run()
{
	consume();
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		produce();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public static void main(String[] args) {
	Console console = System.console();	
    if (console != null) {
    	
        console.writer().close();
        console.printf("Hello!");
    }
	Thread t=new Thread(new Flags2());
	t.start();
}
}
