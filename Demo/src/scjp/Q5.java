package scjp;

public class Q5 implements Runnable {
	static PingPong2 pp2 = new PingPong2();

	public static void main(String[] args) {
		for(int i=0;i<50;i++){
		new Thread(new Q5()).start();
		new Thread(new Q5()).start();
	}}

	public void run() {
		pp2.hit(Thread.currentThread().getId());
	}

}

class PingPong2 {
	synchronized void hit(long n) {
		for (int i = 1; i < 3; i++)
			System.out.println(n + "-" + i + " ");
	}
}