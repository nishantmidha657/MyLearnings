package AbstractClassDemoUsingInnerClass;

import java.security.Timestamp;
import java.util.TreeSet;

public class Test1 {

	
	public static void main(String[] args) {
		TreeSet<Integer> ab=new TreeSet<Integer>();
		ab.add(1205);
		ab.add(1505);
		ab.add(1605);
		ab.add(1705);
		ab.add(1805);
		TreeSet<Integer> subset=new TreeSet<Integer>();
		subset=(TreeSet)ab.headSet(1605);
		System.out.println(subset);
		System.out.println("lastt"+subset.last());
		
		TreeSet<Integer> subset2=new TreeSet<Integer>();
		subset2=(TreeSet)ab.headSet(1605, true);
		
		System.out.println("2"+subset2+"last"+subset2);
		TreeSet<Integer> subset1=new TreeSet<Integer>();
		subset1=(TreeSet)ab.tailSet(1605);
	System.out.println(subset1);	
	}
}
