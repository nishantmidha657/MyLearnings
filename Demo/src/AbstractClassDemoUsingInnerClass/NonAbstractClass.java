package AbstractClassDemoUsingInnerClass;

public class NonAbstractClass {

	public void displayMethod() {
		System.out.println("In NonAbstract class:Display method called");

	}

	public void ExcptionMethod() {
		System.err.print("Exception block executed");
	}

	void storeData(final int a)
	{
		new Interceptor()
		{

			@Override
			void invokeMethod() {
			
				DAOclass daOclass=new DAOclass();
				daOclass.storeData(a);	
			}
			
		}.execute();;
	}
	abstract  class Interceptor {
		abstract void invokeMethod();

		void execute() {
			displayMethod();
			try {
				invokeMethod();
			} catch (IllegalArgumentException e) {
				System.out.println("Illegal argument exception thrown");

			} catch (Exception exception) {

			}
		}
	}

}
