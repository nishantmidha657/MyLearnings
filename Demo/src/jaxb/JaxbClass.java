package jaxb;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JaxbClass {
	
public static void main(String[] args) throws JAXBException {
	long start =System.currentTimeMillis();
	unmarchal();
	System.out.println(System.currentTimeMillis()-start);
}

void marchal() throws JAXBException
{
	System.out.println();
	File file=new File("d:\\file.xml");
	PushServerXmlContent pushServerXmlContent=new PushServerXmlContent();
	pushServerXmlContent.setDeviceId("AAAAAA");
	pushServerXmlContent.setMsisdn("1234");
	pushServerXmlContent.setId("IS");
	JAXBContext jaxbContext=JAXBContext.newInstance(PushServerXmlContent.class);
	
	Marshaller marshaller=jaxbContext.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	marshaller.marshal(pushServerXmlContent, file);
}

static void unmarchal() throws JAXBException
{	File file=new File("d:\\file.xml");
JAXBContext jaxbContext=JAXBContext.newInstance(PushServerXmlContent.class);
Unmarshaller unmarshaller=jaxbContext.createUnmarshaller();
PushServerXmlContent content=(PushServerXmlContent)unmarshaller.unmarshal(file);
System.out.println(content.toString());


	
}
}
