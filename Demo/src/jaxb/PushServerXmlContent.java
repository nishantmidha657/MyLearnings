package jaxb;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PushServerXmlContent {

	String msisdn;
	String deviceId;
	String Id;
	public String getId() {
		return Id;
	}
@XmlAttribute
	public void setId(String id) {
		Id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}
	
	@XmlElement
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getDeviceId() {
		return deviceId;
	}
	@XmlElement
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
