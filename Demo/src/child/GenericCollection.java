package child;

import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xml.internal.resolver.Catalog;

public class GenericCollection< T extends Animal> {
public static void main(String[] args) {

	Animal []animals ={new Dog(),new Dog()};
addAnimal(animals);

Dog []dog={new Dog(),new Dog()};
//Result into Array store exception
//addAnimal(dog);

List<Animal> animal=new ArrayList<Animal>();
animal.add(new Dog());
animal.add(new Dog());

addAnimal(animal);


List<Dog> dogList=new ArrayList<Dog>();
dogList.add(new Dog());

addAnimal1(dogList);

//now i can pass any subclass of animal but i cant add anything to that list
//using ? extends Animal
addAnimal2(dogList);

//using ? super Dog 
// now i can pass dog and animal instance 
addAnimal3(dogList);

addAnimal4(dogList);
GenericCollection<Dog> sd=new GenericCollection<Dog>();
sd.createList(new Dog());
}

static void addAnimal(Animal animal[])
{
animal[0]=new Dog();	
animal[1]=new Cat();
}

static void addAnimal(List<Animal> animal)
{
	animal.add(new Dog());
	animal.add(new Cat());
}
static void addAnimal1(List animal)
{
	animal.add(new Dog());
	animal.add(new Cat());
}

static void addAnimal2(List<? extends Animal> animal)
{
	//i cant add anytihng in this calss  
	//animal.add(new Dog());
	//animal.add(new Cat());
}

static void addAnimal3(List<? super Dog> animal)
{
	animal.add(new Dog()); 
	//i cant add anytihng in this calss  
	//animal.add(new Dog());
	//animal.add(new Cat());
}
static void addAnimal4(List<?> animal)
{
//	animal.add(new Dog()); 
	//i cant add anytihng in this calss  
	//animal.add(new Dog());
	//animal.add(new Cat());
}

 void createList(T animal)
{
	System.out.println(animal);
}

}


abstract class Animal {
	abstract void checkup();
}

class Dog extends Animal
{

	@Override
	void checkup() {
		// TODO Auto-generated method stub
		
		System.out.println("Inside dog class ");
		
	}
	
}

class Cat extends Animal
{

	@Override
	void checkup() {
		// TODO Auto-generated method stub
System.out.println("Inside cat c;lass");			
	}
	
}