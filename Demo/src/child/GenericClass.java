package child;

import java.util.ArrayList;

public class GenericClass {

	
	public static void main(String[] args) {
		CreateList<Integer>  b=new CreateList<Integer>();
	ArrayList<Integer> v=	b.addElement(10);
	b.addElement(20);
	System.out.println(v);
	}
}



class CreateList<T>
{
	ArrayList<T> b=new ArrayList<T>();
	
	
	ArrayList<T> addElement(T value)
	{
		b.add(value);
		return b;
	}
	
	
}