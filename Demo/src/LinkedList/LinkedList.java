package LinkedList;

public class LinkedList {

	Node head;
	Node tail;
	
	public LinkedList() {
	this.head=new Node("head");
	tail=head;
	}
	
	void add(Node node)
	{
		tail.next=node;
		tail=node;
	}
	
	public static void main(String[] args) {
		LinkedList list=new LinkedList();
		list.add(new Node("1"));
		list.add(new Node("2"));
		list.add(new Node("3"));
		list.add(new Node("4"));
		list.add(new Node("5"));
	
		
		Node current=list.head;
		Node middle=list.head;
		int length =0;
	while(current.next() != null)
	{
		length++;
	if(length%2 == 0)
	{
		middle=middle.next();
	}
		current=current.next();
	}
	if(length%2 == 1)
	{
		middle=middle.next();
	}
	
	System.out.println("Length of linked list is "+length);
	System.out.println("middle element is "+middle.data());
	}
}
