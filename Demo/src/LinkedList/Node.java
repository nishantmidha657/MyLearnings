package LinkedList;

public class Node {

	String data;
	Node next;
	
	public Node(String data) {
	this.data=data;
	}
	
	public String data()
	{
		return this.data;
	}
	public void setData(String data)
	{
		this.data=data;
	}
	
	public Node next()
	{
		return next;
	}
	
	public void setNext(Node next)
	{
		this.next=next;
	}
	
	public String toString()
	{
		return this.data;
	}
}
