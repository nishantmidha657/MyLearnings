package com.mars.rover;

import com.mars.rover.RoverSession;

public class North extends Direction{

	@Override
	void move(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.getRover().yCordinate++;

		System.out
				.println("Rover "+roverId+"  Moved one step in North Direction current position : (x,y) = ("
						+ rover.getRover().xCordinate
						+ ","
						+ rover.getRover().yCordinate + ")");
		
	}

	@Override
	Name getName() {
		return Name.N;
	}

	@Override
	void Left(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getWestDirection(), roverId);
		
	}

	@Override
	void Right(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getEastDirection(), roverId);
		
	}

}
