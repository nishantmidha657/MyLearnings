package com.mars.rover;

public class RoverManager {

	private static RoverManager roverManager;
	private Plateau plateau;
	private North north;
	private South south;
	private West west;
	private East east;

	public RoverManager() {
		plateau = new Plateau();
		north = new North();
		east = new East();
		south = new South();
		west = new West();
	}

	public static RoverManager getInstance() {
		if (roverManager == null) {

			roverManager = new RoverManager();
			return roverManager;
		} else {
			return roverManager;
		}
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public North getNorthDirection() {
		return north;
	}

	public South getSouthDirection() {
		return south;
	}

	public West getWestDirection() {
		return west;
	}

	public East getEastDirection() {
		return east;
	}

	public int sendRoverOnPleateu(Rover rover, Direction direction) {
		return getPlateau().addRover(rover, direction);

	}

}
