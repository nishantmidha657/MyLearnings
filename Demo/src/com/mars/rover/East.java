package com.mars.rover;

import com.mars.rover.RoverSession;

public class East extends Direction {

	@Override
	void move(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.getRover().xCordinate++;

		System.out
				.println("Rover "+roverId+" Moved one step in East Direction current position : (x,y) = ("
						+ rover.getRover().xCordinate
						+ ","
						+ rover.getRover().yCordinate + ")");
	}

	@Override
	Name getName() {
		// TODO Auto-generated method stub
		return Name.E;
	}

	@Override
	void Left(int roverId) {
		// TODO Auto-generated method stub
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getNorthDirection(), roverId);
	}

	@Override
	void Right(int roverId) {
		// TODO Auto-generated method stub
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getSouthDirection(), roverId);
	}

}
