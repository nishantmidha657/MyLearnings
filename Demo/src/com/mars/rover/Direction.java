package com.mars.rover;

import com.mars.rover.RoverSession;

public abstract class Direction {

	enum Name {
		E, W, N, S;
	}

	void move(int roverId) {
		throw new IllegalStateException("Rover CanNot Move Further");
	}

	void Left(int roverId) {
		throw new IllegalStateException("Rover CanNot Turn left");
	}

	void Right(int roverId) {
		throw new IllegalStateException("Rover CanNot Move Right");
	}

	abstract Name getName();

	synchronized void  changeDirection(Direction direction, int roverId) {
		RoverSession session=RoverManager.getInstance().getPlateau().getRover(roverId);
		Direction oldDirection=session.getDirection();
		session.setDirection(direction);
		System.out.println("Rover " + roverId + "changed the direction "
				+ oldDirection.getName()+" TO "+direction.getName());
	}

}
