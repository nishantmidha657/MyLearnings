package com.mars.rover;

import java.util.HashMap;
import java.util.Map;

public class Plateau {

	Map<Integer, RoverSession> data;

	public Plateau() {
		// TODO Auto-generated constructor stub
		data = new HashMap<Integer, RoverSession>();
	}

	private void putSession(RoverSession session) {

		data.put(session.getRover().getId(), session);

	}

	public int  addRover(Rover rover, Direction direction) {
		RoverSession roverSession = new RoverSession();

		roverSession.setRover(rover);
		roverSession.setDirection(direction);
		putSession(roverSession);
		return rover.getId();
	}

	public RoverSession getRover(int id) {
	
		RoverSession roverSession = data.get(id);
		return roverSession;
	}

}

class RoverSession {
	Rover rover;
	Direction direction;

	public Rover getRover() {
		return rover;
	}

	public void setRover(Rover rover) {
		this.rover = rover;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}