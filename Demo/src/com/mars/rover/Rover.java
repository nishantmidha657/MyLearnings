package com.mars.rover;

public class Rover {

	private static Integer id = 0;

	public int xCordinate;
	public int yCordinate;

	public Rover() {
		id++;
	}

	String name;

	public void setName(String name) {
		this.name = name;

	}

	public String getName(String name) {
		return name;

	}

	public Integer getId() {
		return id;
	}
}
