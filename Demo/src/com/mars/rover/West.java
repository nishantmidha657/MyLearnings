package com.mars.rover;

import com.mars.rover.RoverSession;

public class West extends Direction{

	@Override
	void move(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.getRover().xCordinate--;

		System.out
				.println("Rover "+roverId+"  Moved one step in West Direction current position : (x,y) = ("
						+ rover.getRover().xCordinate
						+ ","
						+ rover.getRover().yCordinate + ")");
		
	}

	@Override
	Name getName() {
		// TODO Auto-generated method stub
		return Name.W;
	}

	@Override
	void Left(int roverId) {
		
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getSouthDirection(), roverId);
	}

	@Override
	void Right(int roverId) {
		RoverSession rover = RoverManager.getInstance().getPlateau()
				.getRover(roverId);
		rover.direction.changeDirection(RoverManager.getInstance()
				.getNorthDirection(), roverId);
		
	}

}
