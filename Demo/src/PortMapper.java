import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class PortMapper {
	
	void getPort2() throws UnknownHostException, IOException
	{
		ServerSocket s=new ServerSocket(5629, 1, InetAddress.getByName("localhost"));
		s.accept();
	}
	
	public static final int MAX_PORT_RANGE = 5640;

	int propertyConfiguredValue = 5630;

	int lastReturnedPort = 0;
	List<Integer> alreadyReturnedPort = new ArrayList<>();

	int getPort(String ipAddress) throws UnknownHostException {
		boolean isPortNotFound = true;
		int startingPort = 0;

		if (lastReturnedPort == 0 || lastReturnedPort == MAX_PORT_RANGE) {
			startingPort = propertyConfiguredValue;
		} else {
			startingPort = lastReturnedPort + 1;
		}

		System.out.println("last returned port was -->" + lastReturnedPort);
		InetAddress ip = InetAddress.getByName("localhost");
		while (isPortNotFound && startingPort <= MAX_PORT_RANGE) {
			try (ServerSocket s = new ServerSocket(startingPort, 1, ip) {
				@Override
				public void close() throws IOException {
					super.close();
					System.out.println("Closing socket for port "
							+ lastReturnedPort + this.isClosed());

				}
			}) {
				lastReturnedPort = s.getLocalPort();
				if (alreadyReturnedPort.contains(lastReturnedPort)) {
					startingPort++;
					if(lastReturnedPort==MAX_PORT_RANGE)
					{
						lastReturnedPort = 0;
						startingPort = propertyConfiguredValue;
						System.out.println("All ports are busy");
					}
					continue;
				}
				isPortNotFound = false;

			} catch (IOException e) {
				if (lastReturnedPort == MAX_PORT_RANGE) {
					lastReturnedPort = 0;
					startingPort = propertyConfiguredValue;
					System.out.println("All ports are busy");
				}
				lastReturnedPort = startingPort;
				startingPort++;

				continue;
			}
		}
		System.out
				.println("New free  port  assigned is -->" + lastReturnedPort);
		alreadyReturnedPort.add(lastReturnedPort);
		return lastReturnedPort;
	}

	void assertLastReturnedWithInMaxRange() {
		if (lastReturnedPort == MAX_PORT_RANGE) {
			// startingPort = propertyConfiguredValue;
			lastReturnedPort = 0;
		}
	}
	
	int getPort3()
	{
		return 123454;
	}
	
}