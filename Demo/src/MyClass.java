
public class MyClass {

	static class Friend{
		private final String name ;
		public Friend(String name){
			this.name=name;
		}
		public String getName(){
			return name;
		}
		
		public synchronized void bow(Friend bower){
			System.out.println(bower.getName());
			bower.bowBack(this);
		}
		public synchronized void bowBack(Friend bower){
			System.out.println(this.name);
			bower.bowBack(this);
			
		}
	}
	
	public static void main(String[] args) {
		final Friend a=new Friend("nishant");
		final Friend b=new Friend("nishant");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				a.bow(b);
			}
		}).start();
		
new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				b.bow(a);
			}
		}).start();

	}
}
