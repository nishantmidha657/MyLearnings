package ThreadPool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;

import com.sun.corba.se.impl.orbutil.closure.Future;
class ThreadPoolExecutor1  {
public static void main(String[] args) throws Exception{
	
	ThreadFactory factory=new ThreadFactory() {
		int i=0;
		@Override
		public Thread newThread(Runnable r) {
			// TODO Auto-generated method stub
			System.out.println("creating therad");
			return new Thread("Thread-"+ i++);
		}
	};
	LinkedBlockingQueue<Runnable> b=new LinkedBlockingQueue<Runnable>(1);
	ThreadPoolExecutor POOL=new ThreadPoolExecutor(2,5,3,TimeUnit.SECONDS,b,factory);
	
	POOL.prestartAllCoreThreads();
	Thread.sleep(3000);
	
	System.out.println(POOL.getActiveCount());
	java.util.concurrent.Future<?> sd=POOL.submit(new WorkerThread("first"));
	POOL.submit(new WorkerThread("first"));
	System.out.println(POOL.getActiveCount());
	System.out.println(sd.isDone());
	POOL.submit(new WorkerThread("first"));
	System.out.println(sd.isDone());
	Thread.sleep(8000);
	System.out.println(POOL.getActiveCount());
	
	
}
}
