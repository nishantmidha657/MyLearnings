package ThreadPool;

public class WorkerThread implements Runnable {

	private String command;

	public WorkerThread(String command) {
		this.command = command;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(Thread.currentThread().getName()
				+ "Start.Command = " + command);
		System.out.println(Thread.currentThread().getName() + "END.Command = "
				+ command);

	}

	private void processCommand() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		return this.command;
	}

}
