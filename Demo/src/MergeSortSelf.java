public class MergeSortSelf {
	int array[] = { 100, 20, 10, 70, 50, 60, 90, 3, 400 };

	public static void main(String[] args) {

		MergeSortSelf mergeSortSelf = new MergeSortSelf();
		int workSpace[] = new int[mergeSortSelf.array.length];

		mergeSortSelf.display();
		mergeSortSelf.mergeSort(workSpace, 0, mergeSortSelf.array.length - 1);

	}

	// 0,11--> 0,5/0,2/0,1/0,0/

	void mergeSort(int workSpace[], int start, int end) {
		System.out.println(String.format(
				"merge sort called with start %s end {%s}", start, end));
		if (start == end) {
			return;
		}

		int mid = (start + end) / 2;

		mergeSort(workSpace, start, mid);

		mergeSort(workSpace, mid + 1, end);

		merge(workSpace, start, mid + 1, end);
	}

	public void merge(int[] workSpace, int lowerBound, int high, int upperBound) {
		int n = 0;
		int j = upperBound - lowerBound + 1;
		int lowerPtr = lowerBound;
		int uppperPtr = upperBound;
		int mid = high - 1;

		while (lowerPtr <= mid && high <= upperBound) {

			if (array[lowerPtr] < array[high]) {
				workSpace[n++] = array[lowerPtr++];
			} else {
				workSpace[n++] = array[high++];
			}

		}

		while (lowerPtr <= mid) {
			workSpace[n++] = array[lowerPtr++];
		}

		while (high <= upperBound) {
			workSpace[n++] = array[high++];
		}

		for (int i = lowerBound; i < j; i++) {
			array[lowerBound + i] = workSpace[i];
		}
	}

	public void display() {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			System.out.print(",");
		}
	}

}
