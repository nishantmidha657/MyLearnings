package packageB;

import java.util.HashSet;

public class Demo {
	
public static void main(String[] args) {
	HashSet set=new  HashSet();
	
	Employee e1=new Employee();
	Employee e2=new Employee();
	e1.setName("nishant");
	e2.setName("nishant");
	Employee e3=e1;
	System.out.print("e1==e2-> ");
	System.out.println(e1==e2);
    System.out.print("e1.equals(e2)->");
	System.out.println(e1.equals(e2));
	
	System.out.print("e1==e3-> ");	
	System.out.println(e1==e3);
	System.out.print("e1.equals(e3)->");
	System.out.println(e1.equals(e3));
	
	System.out.print("hashcode-e1->");
	System.out.println(e1.hashCode());
	System.out.print("hashcode-e2->");
	System.out.println(e2.hashCode());
	System.out.print("hashcode-e3->");
	System.out.println(e3.hashCode());
	
	
	set.add(e1);
	set.add(e2);
	set.add(e3);
	System.out.print("Added(e1,e2,e3) ->");
	System.out.println(set.size());
	
	System.out.println(set.contains(e2));
	set.remove("nishant");
	System.out.println(set.size());
	//changing name s
}


}
