package com.nishant.soap.example;

import net.webservicex.GeoIP;
import net.webservicex.GeoIPService;
import net.webservicex.GeoIPServiceSoap;

public class IpLocationFinder {

	public static void main(String[] args) {
		/*if (args.length != 1) {
			System.out.println("You need to pass in one ipaddress");
		} else*/ {
			String ipAddress = args[0];

			GeoIPService geoIPService = new GeoIPService();
			GeoIPServiceSoap geoIPServiceSoap = geoIPService
					.getGeoIPServiceSoap();

			GeoIP geoIP = geoIPServiceSoap.getGeoIP("212.58.246.78");
			
			System.out.println(geoIP.getCountryName());

		}
	}
}
